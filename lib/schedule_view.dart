import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sed/home_view.dart';
import 'package:sed/main.dart';
import 'package:lottie/lottie.dart';

class ScheduleView extends StatefulWidget {
  const ScheduleView({super.key});

  @override
  State<ScheduleView> createState() => _ScheduleViewState();
}

class _ScheduleViewState extends State<ScheduleView> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            const SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.white,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Schedule',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, fontFamily: ''),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: mainColor,
                          )),
                      ...buildListDay(),
                      IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.arrow_forward_ios,
                            color: mainColor,
                          )),
                    ],
                  ),
                ],
              ),
            ),
            Lottie.asset(
              'assets/images/schedule.json',
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Text(
                    'Sleep: 22:00 ~ 5:00',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, fontFamily: ''),
                  ),
                  Lottie.asset(
                    'assets/images/sleep.json',
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Text(
                    'Sleep: 12:00 ~ 12:30',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, fontFamily: ''),
                  ),
                  Lottie.asset(
                    'assets/images/sleep.json',
                  ),
                ],
              ),
            ),
            ...timeOfDay
                .map((e) => Container(
                      margin: EdgeInsets.symmetric(vertical: 8),
                      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40),
                        color: Colors.white,
                      ),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Center(
                                  child: Text(
                                    '${title[timeOfDay.indexOf(e)]}: ${e.hour}:${e.minute}\n${content[timeOfDay.indexOf(e)]}',
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, fontFamily: ''),
                                  ),
                                ),
                              ),
                              IconButton(
                                  onPressed: () {
                                    int index = timeOfDay.indexOf(e);
                                    timeOfDay.removeAt(index);
                                    title.removeAt(index);
                                    content.removeAt(index);
                                    setState(() {});
                                  },
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ))
                            ],
                          ),
                          Lottie.asset(
                            'assets/images/custom.json',
                          ),
                        ],
                      ),
                    ))
                .toList(),
          ],
        ),
      ),
    );
  }

  List<Widget> buildListDay() {
    DateTime now = DateTime.now();
    final List<Widget> list = [];
    final DateTime time = now.subtract(Duration(days: now.weekday - 1));
    for (int i = 0; i < 7; i++) {
      list.add(
        buildDay(
          time.add(
            Duration(days: i),
          ),
        ),
      );
    }

    return list;
  }

  Widget buildDay(DateTime time) {
    DateTime now = DateTime.now();
    final bool isPick = time.difference(now).inDays == 0;
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 4, vertical: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: isPick ? mainColor : Colors.transparent,
        ),
        child: Column(
          children: [
            Text(
              DateFormat('EEE').format(time),
              style: isPick ? TextStyle(color: Colors.white) : TextStyle(color: Colors.grey),
            ),
            SizedBox(
              height: 4,
            ),
            Text(
              DateFormat('dd').format(time),
              style: isPick ? TextStyle(color: Colors.white) : TextStyle(color: Colors.grey),
            ),
          ],
        ),
      ),
    );
  }
}
