import 'package:flutter/material.dart';
import 'package:sed/home_view.dart';
import 'package:sed/main.dart';

class LoginView extends StatefulWidget {
  const LoginView({super.key});

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  bool isScure = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Center(
          child: Container(
            padding: const EdgeInsets.all(40),
            margin: const EdgeInsets.symmetric(horizontal: 40),
            decoration: BoxDecoration(
              color: Colors.white70,
              borderRadius: BorderRadius.circular(30),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Hi,',
                  style: TextStyle(
                    fontSize: 32,
                    color: mainColor,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'overpass',
                  ),
                ),
                Text(
                  'Welcome back.',
                  style: TextStyle(
                    fontSize: 32,
                    color: mainColor,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'overpass',
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Text(
                  'User name or email:',
                  style: Theme.of(context).textTheme.labelMedium,
                ),
                const SizedBox(
                  height: 16,
                ),
                TextField(
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(64),
                      borderSide: const BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(64),
                      borderSide: BorderSide(
                        color: mainColor,
                      ),
                    ),
                    hintText: 'username@email.com',
                    contentPadding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 16),
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Text(
                  'Password:',
                  style: Theme.of(context).textTheme.labelMedium,
                ),
                const SizedBox(
                  height: 16,
                ),
                TextField(
                  obscureText: isScure,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(64),
                      borderSide: const BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(64),
                      borderSide: BorderSide(
                        color: mainColor,
                      ),
                    ),
                    hintText: 'password',
                    contentPadding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 16),
                    suffixIcon: IconButton(
                      onPressed: () {
                        isScure = !isScure;
                        setState(() {});
                      },
                      icon: Icon(
                        isScure ? Icons.visibility : Icons.visibility_off,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 4,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'Forgot password?',
                    style: TextStyle(
                      fontSize: 16,
                      color: mainColor,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const HomeView()),
                    );
                  },
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            gradient: LinearGradient(
                                colors: [secondaryColor, mainColor]),
                          ),
                          child: const Center(
                            child: Text(
                              'Login',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
