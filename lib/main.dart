import 'package:flutter/material.dart';
import 'package:sed/login_view.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('ic_launcher');
  const DarwinInitializationSettings initializationSettingsDarwin =
      DarwinInitializationSettings();
  const InitializationSettings initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsDarwin,
    macOS: initializationSettingsDarwin,
  );
  await flutterLocalNotificationsPlugin.initialize(
    initializationSettings,
    onDidReceiveNotificationResponse: (response) {},
  );
  runApp(const MyApp());
}

Color mainColor = const Color.fromRGBO(95, 70, 216, 1);
Color secondaryColor = const Color.fromRGBO(157, 99, 246, 1);
Color backgroundColor = const Color.fromRGBO(231, 233, 246, 1);

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'SED',
      // theme: ThemeData(
      //   primarySwatch: Colors.purple,
      //   fontFamily: 'overpass',
      //   colorScheme: ColorScheme(
      //     brightness: Brightness.light,
      //     primary: mainColor,
      //     onPrimary: Colors.white,
      //     secondary: secondaryColor,
      //     onSecondary: Colors.white,
      //     error: Colors.red,
      //     onError: Colors.red,
      //     background: backgroundColor,
      //     onBackground: backgroundColor,
      //     surface: mainColor,
      //     onSurface: Colors.white,
      //   ),
      // ),
      home: const LoginView(),
    );
  }
}
