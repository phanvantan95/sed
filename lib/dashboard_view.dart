import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:sed/chart.dart';
import 'package:sed/main.dart';
import 'package:lottie/lottie.dart';
import 'package:url_launcher/url_launcher.dart';

class DashboardView extends StatefulWidget {
  const DashboardView({super.key});

  @override
  State<DashboardView> createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(
            height: 8,
          ),
          Container(
            padding: const EdgeInsets.all(16),
            margin: const EdgeInsets.symmetric(horizontal: 16),
            decoration: BoxDecoration(
              color: Colors.white70,
              borderRadius: BorderRadius.circular(30),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 16,
                ),
                const Text(
                  'Sleep Avg',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, fontFamily: ''),
                ),
                const Chart(),
                const SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          const Text(
                            'Activity Avg',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Text(
                            '2.5h',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: mainColor,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          const Text(
                            'Calo Avg',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          const Text(
                            '800 calo',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.pink,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          const Text(
                            'Sleep Avg',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          const Text(
                            '8.5h',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.yellow,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          const Text(
                            'Happy Avg',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          const Text(
                            '8.5',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Text(
                  'Target',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Slider(
                  value: 80,
                  min: 0,
                  max: 100,
                  onChanged: (value) {},
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Lottie.asset(
            'assets/images/target.json',
          ),
          const SizedBox(
            height: 8,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'SẮC ĐẸP',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Xem tất cả',
                  style: TextStyle(
                    color: mainColor,
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://ionia.com.vn/loi-khuyen-tu-cac-chuyen-gia-hang-dau-ve-suc-khoe-sac-dep'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://ionia.com.vn/wp-content/uploads/2021/03/khoe-dep-co-tot-675x357.jpg',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              'Lời khuyên của các chuyên gia về chăm sóc sắc đẹp',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://seoulspa.vn/cach-de-xinh-dep-hon'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://cdn.diemnhangroup.com/seoulspa/2023/02/cach-de-xinh-dep-hon-3.jpg',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              'Những việc làm cơ bản hàng ngày để giúp bạn trở nên xinh đẹp hơn',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://medlatec.vn/tin-tuc/5-tuyet-chieu-lam-dep-da-tu-nhien-cuc-don-gian-de-thuc-hien-s107-n20333'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://login.medlatec.vn//ImagePath/images/20201103/20201103_lam-dep-da-1.jpg',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              'Bí quyết để có làn da đẹp tự nhiên',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://hannaholala.com/blogs/beauty/lan-da-thay-doi-nhu-the-nao-trong-24h'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://file.hstatic.net/1000312510/file/1_5ee9953feab5482cbba82f007a47ada1_grande.jpg',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              'Làn da thay đổi như thế nào trong 24h',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://hannaholala.com/blogs/beauty/cham-soc-da-va-trang-diem-khi-mang-bau'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://file.hstatic.net/1000312510/file/3_3bbc060857c148b5aed0b13b934313e7_grande.jpg',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              'Chăm sóc da và trang điểm khi mang bầu',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://hannaholala.com/blogs/beauty/cach-cham-soc-da-nhay-cam'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://file.hstatic.net/1000312510/file/1_167c88bdb8b742d6bb684f18ad2206f6_grande.jpg',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              'Cách chăm sóc da nhạy cảm',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'THỰC PHẨM VÀ DINH DƯỠNG',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Xem tất cả',
                  style: TextStyle(
                    color: mainColor,
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://medlatec.vn/amp/tin-tuc/9-nguyen-tac-can-luu-y-khi-theo-che-do-an-healthy-s51-n30014'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://login.medlatec.vn//ImagePath/images/20220917/20220917_che-do-an-healthy-1.png',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              '9 nguyên tắc cần lưu ý khi theo chế độ ăn healthy',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://amp.laodong.vn/suc-khoe/10-loi-khuyen-don-gian-lam-cho-che-do-an-uong-cua-ban-lanh-manh-hon-924526.ldo'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://media-cdn-v2.laodong.vn/Storage/NewsPortal/2021/6/26/924526/Received_78752898862-01.jpeg',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              '10 lời khuyên đơn giản làm cho chế độ ăn uống của bạn lành mạnh hơn',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    launchUrl(
                      Uri.parse('https://www.bachhoaxanh.com/kinh-nghiem-hay/thuc-don-giam-can-nhanh-nhat-ma-lai-an-toan-khong-bi-met-1191178'),
                      mode: LaunchMode.externalApplication,
                    );
                  },
                  child: Card(
                    child: SizedBox(
                      height: 150,
                      width: 150,
                      child: Column(
                        children: [
                          Image.network(
                            'https://cdn.tgdd.vn/Files/2019/08/24/1191178/thuc-don-giam-can-nhanh-nhat-ma-lai-an-toan-khong-bi-met-201908241527220455.jpg',
                            height: 100,
                            fit: BoxFit.fill,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                            child: Text(
                              'Thực đơn giảm cân nhanh nhất mà lại an toàn, không bị mệt',
                              style: TextStyle(fontSize: 14),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 60,
          ),
        ],
      ),
    );
  }
}
