import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sed/dashboard_view.dart';
import 'package:sed/fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:sed/main.dart';
import 'package:sed/profile_view.dart';
import 'package:sed/schedule_view.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

List<TimeOfDay> timeOfDay = [];
List<String> title = [];
List<String> content = [];

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  int tabIndex = 0;
  late Timer timer;

  @override
  void initState() {
    super.initState();

    int delay = 62 - DateTime.now().second;

    Future.delayed(Duration(seconds: delay), () {
      timer = Timer.periodic(Duration(minutes: 1), (timer) async {
        DateTime now = DateTime.now();
        if (now.hour == 12 && now.minute == 0) {
          const AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails('your channel id', 'your channel name',
              channelDescription: 'your channel description', importance: Importance.max, priority: Priority.high, ticker: 'ticker');
          const NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
          await flutterLocalNotificationsPlugin.show(0, 'Thông báo', 'Đã tới giờ ngủ trưa 12h00 -> 12h30', notificationDetails, payload: 'item x');
        }
        if (now.hour == 22 && now.minute == 0) {
          const AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails('your channel id', 'your channel name',
              channelDescription: 'your channel description', importance: Importance.max, priority: Priority.high, ticker: 'ticker');
          const NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
          await flutterLocalNotificationsPlugin.show(0, 'Thông báo', 'Đã tới giờ ngủ trưa 12h00 -> 12h30', notificationDetails, payload: 'item x');
        }

        for (int i = 0; i < timeOfDay.length; i++) {
          print(DateTime.now());
          TimeOfDay time = timeOfDay[i];
          DateTime timeTemp = DateTime(2023, 1, 1, time.hour, time.minute);
          DateTime timeBefore = timeTemp.subtract(Duration(minutes: 15));
          TimeOfDay timeBeforeDay = TimeOfDay(hour: timeBefore.hour, minute: timeBefore.minute);
          if (timeOfDay[i].hour == now.hour && timeOfDay[i].minute == now.minute) {
            const AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails('your channel id', 'your channel name',
                channelDescription: 'your channel description', importance: Importance.max, priority: Priority.high, ticker: 'ticker');
            const NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
            await flutterLocalNotificationsPlugin.show(0, title[i], content[i], notificationDetails, payload: 'item x');
          }
          if ((timeBeforeDay.hour == now.hour && timeBeforeDay.minute == now.minute)) {
            const AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails('your channel id', 'your channel name',
                channelDescription: 'your channel description', importance: Importance.max, priority: Priority.high, ticker: 'ticker');
            const NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
            await flutterLocalNotificationsPlugin.show(0, "15 minutes to " + title[i], content[i], notificationDetails, payload: 'item x');
          }
        }
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      bottomNavigationBar: FancyBottomNavigation(
        tabs: [
          TabData(
            iconData: Icons.dashboard,
            title: "Dashboard",
          ),
          TabData(
            iconData: Icons.schedule,
            title: 'Schedule',
          ),
          TabData(
            iconData: Icons.person,
            title: 'Profile',
          ),
        ],
        initialSelection: tabIndex,
        onTabChangedListener: (index) {
          setState(() {
            tabIndex = index;
          });
        },
        activeIconColor: Colors.white,
        inactiveIconColor: mainColor,
        circleColor: mainColor,
        textColor: mainColor,
      ),
      floatingActionButton: tabIndex == 1
          ? FloatingActionButton(
              backgroundColor: mainColor,
              onPressed: () async {
                TimeOfDay? selectedTime = await showTimePicker(
                  initialTime: TimeOfDay.now(),
                  context: context,
                );
                if (selectedTime != null) {
                  String titleT = '';
                  String contentT = '';
                  showDialog<void>(
                    context: context,
                    barrierDismissible: false, // user must tap button!
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text('Thêm việc'),
                        content: SingleChildScrollView(
                          child: ListBody(
                            children: <Widget>[
                              Text('Thời gian: ${selectedTime.hour} : ${selectedTime.minute}'),
                              SizedBox(
                                height: 4,
                              ),
                              Text('Tiêu đề'),
                              SizedBox(
                                height: 4,
                              ),
                              TextField(
                                onChanged: (value) {
                                  titleT = value;
                                },
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text('Nội dung'),
                              SizedBox(
                                height: 4,
                              ),
                              TextField(
                                onChanged: (value) {
                                  contentT = value;
                                },
                              ),
                              SizedBox(
                                height: 4,
                              ),
                            ],
                          ),
                        ),
                        actions: <Widget>[
                          TextButton(
                            child: const Text('Hủy'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          TextButton(
                            child: const Text('Thêm'),
                            onPressed: () {
                              timeOfDay.add(selectedTime);
                              title.add(titleT);
                              content.add(contentT);
                              Navigator.of(context).pop();
                              setState(() {});
                            },
                          ),
                        ],
                      );
                    },
                  );
                }
              },
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            )
          : null,
      body: SafeArea(
        child: tabIndex == 0
            ? DashboardView()
            : tabIndex == 1
                ? ScheduleView()
                : ProfileView(),
      ),
    );
  }
}
